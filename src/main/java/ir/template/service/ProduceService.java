package ir.template.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class ProduceService {

    private final RabbitTemplate rabbitTemplate;


    public ProduceService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }


    public void sendMessage(String queue, String message){
        rabbitTemplate.convertAndSend(queue, message);
    }

}
