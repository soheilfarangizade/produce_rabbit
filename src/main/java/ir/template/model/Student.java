package ir.template.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class Student implements Serializable {

    private Integer id;
    private String name;
    private List<String> courses;

}
