package ir.template.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import ir.template.model.Student;
import ir.template.service.ProduceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@RestController
@RequestMapping(value = "/produce")
public class ProducerController {

    private final ProduceService produceService;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public ProducerController(ProduceService produceService) {
        this.produceService = produceService;
    }


    @RequestMapping(value = "/send/{counter}", method = RequestMethod.GET)
    public String sendMessage(@PathVariable Integer counter) throws Exception {
        for (int i = 0; i < counter; i++) {
            Student student = Student.builder()
                    .id(i)
                    .name("student Name " + i)
                    .courses(Arrays.asList("list1", "list2"))
                    .build();
            String message = objectMapper.writeValueAsString(student);
            produceService.sendMessage("q.test", message);
        }
        return "ok";
    }
}
