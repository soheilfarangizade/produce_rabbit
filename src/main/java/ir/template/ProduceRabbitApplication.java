package ir.template;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProduceRabbitApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProduceRabbitApplication.class, args);
    }

}
